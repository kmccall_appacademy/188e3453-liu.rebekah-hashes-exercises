# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  result = Hash.new
  str.split.each do |word|
    result[word] = word.length
  end
  result
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|key, value| value}.last.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, value|
    older[key] = value
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  counter = Hash.new(0)
  word.each_char do |ch|
    counter[ch] += 1
  end
  counter
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_hash = Hash.new(0)
  arr.each do |val|
    uniq_hash[val] += 1
  end
  uniq_hash.values.sort
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  counter = Hash.new(0)
  numbers.each do |num|
    counter[:even] += 1 if num.even?
    counter[:odd] += 1 if num.odd?
  end
  counter
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_count = Hash.new(0)
  string.each_char.select {|ch| "aeiouAEIOU".include?(ch)}.each do |ch|
    vowel_count[ch] += 1
  end
  vowel_count.sort_by {|k,v| v}.last.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  fall_students = students.select {|key, value| value > 6}.keys
  combination = []
  fall_students.each_with_index do |name1, ind1|
    fall_students.each_with_index do |name2, ind2|
      next if ind2 <= ind1
      combination << [name1,name2]
    end
  end
  combination
end

def fall_and_winter_birthday?(name)

end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  census = Hash.new(0)
  specimens.each do |animal|
    census[animal] += 1
  end
  sorted_census = census.sort_by {|key, value| value}
  smallest_pop = sorted_census.first.last
  largest_pop = sorted_census.last.last
  census.length**2 * smallest_pop / largest_pop
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_ch = character_count(normal_sign)
  vandalized_sign_ch = character_count(vandalized_sign)
  vandalized_sign_ch.each do |ch, num|
    return false if normal_sign_ch[ch] < num
  end
  true
end

def character_count(str)
  counter = Hash.new(0)
  str.downcase.each_char {|ch| counter[ch] += 1}
  counter
end
